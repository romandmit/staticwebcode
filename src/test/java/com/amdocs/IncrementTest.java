package com.amdocs;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IncrementTest {

    public IncrementTest() {
    }

    @Test
    public void testZero() {
        final Increment increment = new Increment();
        assertEquals("test zero", 0, increment.decreasecounter(0));
    }

    @Test
    public void testOne() {
        final Increment increment = new Increment();
        assertEquals("test one", 1, increment.decreasecounter(1));
    }

    @Test
    public void testTwo() {
        final Increment increment = new Increment();
        assertEquals("test two", 2, increment.decreasecounter(2));
    }
}
