package com.amdocs;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    public CalculatorTest() {
    }

    @Test
    public void testAdd() {
        final Calculator calculator = new Calculator();
        assertEquals("test add", 9, calculator.add());
    }

    @Test
    public void testSub() {
        final Calculator calculator = new Calculator();
        assertEquals("test sub", 3, calculator.sub());
    }
}
